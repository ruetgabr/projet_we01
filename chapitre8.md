# Chapitre 8 : Les alternatives aux GAFAM et modèles économiques dominants du web

[[_TOC_]]

## 1. Le libre et la décentralisation (Salma)

#### Les licences libres et ouvertes

Le développement des licences libres est une forme de la lutte contre les enclosures du domaine intellectuel. La protection du système GNU (1983) par la licence GPL (General Public Licence, 1989), a créé un commun à partir de la définition des droits et devoirs des usagers instaurant un régime juridique de la propriété intellectuelle commune pour protéger et délimiter la communauté d'usage et de production, notamment avec la clause de copyleft et les libertés suivantes:
* liberté d'utiliser le logiciel quelque soit l'usage
* liberté d'étudier le code source et de le modifier
* liberté de le redistribuer sous forme de copies pour aider son voisin
* liberté de redistribuer aux autres vos propres versions modifiées

Les outils de développement GNU ont fait leurs preuves dans les milieux les plus exigeants (industrie du logiciel et instituts de recherche). Ils constituent la fondation des systèmes d'exploitation libres actuels, comme le système Linux.

Quelques années plus tard, le projet Creative Commons est lancé en 2003 en France. Son slogan: "faire sans contrefaire, partager, remixer, réutiliser légalement". Creative Commons propose des licences permettant aux auteurs de mettre leurs oeuvres à disposition du public selon des conditions prédéfinies. Ces licences s'articulent autour des quatre options suivantes:
* **attribution**: cela signifie que ceux qui utilisent votre oeuvre peuvent la reproduire, la distribuer et la communiquer librement, à condition qu'ils vous en attribuent la paternité. Cette condition est commune à toutes les licences CC.
* **pas d'utilisation commerciale**: vous autorisez ceux qui utilisent votre oeuvre à la reproduire, à la diffuser ou à la modifier, mais uniquement dans un but non commercial.
* **partage dans les mêmes conditions**: votre oeuvre peut être reproduite et modifiée pour créer des oeuvres dérivées mais celles-ci devront être publiées sous les mêmes conditions que votre oeuvre original. 
* **pas de modification**: vous autorisez la reproduction et la diffusion uniquement de l'original de votre oeuvre, sans modification. Votre accord est nécessaire à toute traduction, altération, transformation ou ré-utilisation dans une autre oeuvre.

Ces options sont combinées différemment au sein de 6 licences principales.  

Inhérentes au boulversement numérique et au Big Data, des licences sont également développées pour les données. En effet, l'Open Knowledge Foundation (OKF) s'y emploie, avec son projet Open Data Commons pour la protection des données et la création des licences Open Database License (ODbL) qui favorise la libre circulation des données et la licence Open Data Commons Public Domain Dedication and License (PDDL) qui place les données dans le domaine public en renonçant à tous les droits.

#### Les ressources éducatives libres

Préconisées en 2002 par l'UNESCO qui regroupe ses propres productions éducatives libres sur un site dédié, "les ressources éducatives libres offrent une opportunité stratégique pour améliorer la qualité de l'éducation, faciliter le dialogue politique et partager les connaissances et le renforcement des capacités". L'organisation a aussi publié en 2011 des recommendations pour l'utilisation des REL dans l'enseignement supérieur. La définition établie par l'organisme mondial est la suivante: "les ressources éducatives libres sont des matériaux d'enseignement, d'apprentissage ou de recherche appartenant au domaine public ou publiés avec une licence de propriété intellectuelle permettant leur utilisation, adaptation et distribution à titre gratuit".

Les REL proviennent principalement des institutions et des enseignants. Il s'agit donc globalement de conférences, des cours mis en ligne, d'exercices...
La plateforme FUN (France Université Numérique) via son moteur de recherche donne accès gratuitement à plus de 30 000 ressources pédagogiques. Il existe aussi les URFIST (Unité Régionale de Formation à l'Information Scientifique et Technique), des organismes de formation qui militent depuis plus de 20 ans pour un libre accès à la connaissance, le logiciel libre et la neutralité du Net. Toutes leurs productions sont placées sous licence CC.

### Vers une décentralisation

#### Sites web décentralisés

Dans le cadre de l'Internet décentralisé, qualifié également de web 3.0, la décentralisation doit a minima permettre une large autonomie vis-à-vis de certains acteurs habituels dans le cadre de la mise en place de sites internet. Ainsi, un site rééllement décentralisé sera impossible à faire disparaître par la volonté de son hébergeur par exemple ou à être effacé des moteurs de recherches. Mais aussi, l'hébergement d'un site décentralisé et sa diffusion ne doivent pas être centralisés en un seul point mais distribués au maximum au travers d'un grand nombre de noeuds du réseau.

Les sites décentralisés utilisent généralement des protocoles décentralisés qui proposent des services similaires ou complètement différents de ce dont nous avons l'habitude. 

#### Le protocole IPFS

L'IFPS (InterPlanetary File System) est un protocole de gestion de fichiers distribués. Il est comparable à la norme HTTP. En revanche, IPFS fonctionne différemment puisqu'au lieu de communiquer les fichiers à des serveurs précis, il interagit avec le réseau par rapport au fichier en question. Il permet donc de mettre plus facilement en place des sites qui n'ont plus à être hébergés sur un serveur précis mais distribués au travers d'un réseau. Il est donc potentiellement une alternative au fonctionnement actuel du web. Mais aussi, les données déployées sur le protocole sont stockées non pas de façon permanente mais uniquement dans la mesure où la sauvegarde est utile du point de vue réseau. 

Différentes applications décentralisées sont déjà développées en utilisant le protocole IPFS comme Steemit ou OpenBazaar qui est une plateforme d'e-commerce décentralisée.

#### Les noms de domaines décentralisés

Il existe des services décentralisés de noms de domaines. Le plus connu est Ethereum Name Service (ENS). A l'heure actuelle, ce service n'est pas fonctionnel pour tous les navigateurs mais il suffit aujourd'hui de disposer de l'extension web Metamask. 

#### Les moteurs de recherche décentralisés

Aujourd'hui, l'immense majorité des recherches sur internet se font sur Google, le moteur de recherche dominant depuis quelques années.
Pourtant, il existe des alternatives qui proposent des fonctionnalités comme une plus grande protection de la vie privée ou la réutilisation des bénéfices à des fins écologiques ou solidaires. Malheureusement, pour l'heure, aucun d'entre eux n'est en mesure de rivaliser avec le géant californien. Cette situation de monopole est lourde de conséquences. Elle va de pair avec la problématique de l'indexation et de la visibilité. En effet, l'algorithme qui décide de ce qui va être vu de ce qui ne va pas l'être peut porter préjudice arbitrairement à certains sites internet.

Le développement de moteurs de recherche alimenté par la communauté dont l'algorithme serait transparent et calibré par la communauté pourrait constituer la prochaine pierre à apporter à l'édifice du web 3.0. Rendre public ce dernier et inciter la communauté à être acteur en sélectionnant les critères de mise en avant des sites pourrait potentiellement résoudre des problématiques auxquelles les utilisateurs font face aujourd'hui.


*Sources:*
* http://sweetux.org/culture-libre/
* https://blockchainfrance.net/2018/05/22/comprendre-le-web-decentralise/
* https://www.thecointribune.com/actualites/lemergence-du-web-3-0-les-sites-internet-decentralises/
* https://usbeketrica.com/fr/article/web-decentralise-dystopie
* https://eduscol.education.fr/numerique/dossier/competences/communs-information-connaissance/culture-libre-et-acces-ouvert
* https://www.andlil.com/la-culture-libre-a-lere-du-numerique-un-droit-controverse-et-aux-enjeux-multiples-164986.html

## 2. Les CHATONS : acteurs proposants des alternatives concrètes aux GAFAM (Pierre)

### Des chatons pour sauver notre vie privée ?

Derrière l'acronyme CHATONS se trouve le [*Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires*](https://chatons.org/) : un ensemble de petites structures (associations, micro-entreprises, SCOP, particulier ou groupe d'amis) rassemblées à l'initiative de l'association [Framsoft](https://framasoft.org/) pour proposer des services en ligne alternatifs à ceux des GAFAM.

En premier lieu, ces structures *hébergent* des serveurs sur lesquels sont installés les applications proposées et gèrent les données de leurs utilisateurs et utilisatrices. Le respect de la vie privée de ces derniers est garanti par l'engagement de chaque chaton, signataire d'une [charte](https://chatons.org/fr/charte) et d'un [manifeste](https://chatons.org/fr/manifeste), à renoncer à la publicité, au traitement et à l'analyse des données stockées, ainsi qu'à l'utilisation de solutions libres et transparentes.

Un autre trait caractéristique de ce collectif est sa volonté de contrer la centralisation conséquente de l'utilisation massive des services des GAFAM, en fédérant des structures réparties dans différentes régions.  
**Un parallèle intéressant est souvent établi avec l'agriculture** : les CHATONS joueraient ainsi le rôle d'AMAP du numérique proposant des services éthiques et solidaires au plus proche des citoyens et citoyennes de manière alternative aux GAFAM associées à l'agriculture intensive dans un marché mondialisée.

Ainsi, les chatons proposent différents types de services en ligne :
- Des solutions d'hébergement ou d'accès (VPS, VPN, hébergement sur serveurs mutualisés)
- Des solutions de partage de fichiers (envoi et stockage d'images, de vidéos, d'image ou aussi leur diffusion en direct)
- Des solutions de gestion de projet et outils de travail (questionnaire, planification de rendez-vous, *todo list*, agenda partagé, tableur, tableau blanc ou pad collaboratifs...)
- Des solutions de communication (messagerie instantanée, mail, audio et visioconférence, réseau social, blog, liste de diffusion...)
- D'autres outils web variés (Raccourcisseur d'URL, messagerie chiffrée, gestionnaire de mots de passe, forge logicielle, cartographie...)


### De "*Dégooglisons* internet" à "*Déframatisons* internet"

Proposer des services alternatifs au GAFAM ? Avant d'initier les CHATONS en 2016, cet objectif était déjà porté par Framasoft en particulier dans le cadre de sa campagne "[Dégooglisons internet](https://degooglisons-internet.or)" lancée en 2014, un an après les révélations d'[Edward Snowden](https://fr.wikipedia.org/wiki/Edward_Snowden) sur les programmes de surveillance de masse menées par le gouvernements britannique et la NSA.

![Péhä-Banquet-CC-By-1920.jpg](/img/Peha-Banquet-CC-By-1920.jpg "Péhä Banquet")
*Banquet de Péhä, licence CC-BY*

Ce projet a abouti à la mise en ligne de nombreux guides sur les bonnes pratiques numériques, sur la culture du libre, sur l'auto-hébergement, *etc*. Mais aussi d'une trentaine de services en ligne respectueux de la vie privée et correspondant aux valeurs du mouvement, dont le plus utilisé est [Framadate](https://framadate.org/), un outil de planification de rendez-vous.

Lucide et droit dans ses bottes, Framasoft s'est fixé un nouveau cap après le succès de cette campagne (environ 500 000 utilisateurs et utilisatrices par mois des services Framasoft en 2017) : pour ne pas reproduire l'effet silo justement dénoncé chez les GAFAM, l'association annonce en 2019 la fin d'une majorité de ses services dans un billet de blog intitulé ironiquement "[Déframatisons internet](https://framablog.org/2019/09/24/deframasoftisons-internet/)".

Les CHATONS sont un beau levier pour avancer sur ce chemin d'hébergement décentralisé à l'échelle locale. Un portail d'accès simplifié a été créé pour rassembler les principales alternatives aux GAFAM proposées par les CHATONS, n'attendez plus pour vous rendre sur : [entraide.chatons.org](entraide.chatons.org).

De son côté, Framasoft poursuit son exploration d'un monde numérique alternatif respectueux de l'humain et favorisant la diversité. Avec l'initiative [Contributopia](https://contributopia.org/fr/), l'association poursuit trois buts ; *Créer et proposer des outils*, *Transmettre les savoir-faire* et *Inspirer les possibles* :
>  Reste un monde encore bien inconnu à co-créer et défricher ensemble, garni d’outils pensés pour développer de nouvelles façons de faire en commun, de faire des communs. Un monde qui s’inspire des pratiques de nombreuses communautés (dont celles du logiciel libre), pour aider chacun·e à choisir les outils qui lui correspondent, atteindre l’autonomie numérique, collaborer différemment et partager les connaissances

### Picasoft, un chaton à l'UTC !

Picasoft est une association de l'Université de Technologie de Compiègne fondée en 2016 par des étudiants et des enseignants. Elle s'est fixée pour objectif de

>  promouvoir et défendre une approche libriste, inclusive, respectueuse de la vie privée, respectueuse de la liberté d'expression, respectueuse de la solidarité entre les humains et respectueuse de l'environnement, notamment dans le domaine de l'informatique.

Picasoft a fait partie de la troisième portée de chatons, annonçant son inscription parmis les membres du collectif la 7 juillet 2017.

L'association propose aujourd'hui une dizaine de services utilisés par près de 6000 internautes ravis (... ou *a minima* un ici !).



------------
*Sources :*

- https://www.nextinpact.com/article/29667/108225-deframatisons-internet-framasoft-fermera-progressivement-30-services
- https://www.lemonde.fr/pixels/article/2019/12/27/chez-framasoft-des-chatons-pour-sortir-des-gafa_6024230_4408996.html
- https://usbeketrica.com/fr/article/framasoft-une-amap-du-logiciel-libre
- https://linuxfr.org/news/bienvenue-a-la-troisieme-portee-de-chatons
- https://picasoft.net
- https://contributopia.org/fr/




## 3. Modèle du don (crowdfunding) (Edgar)

### Définition et principes

Le *crowdfunding* (ou financement participatif en français) permet à un porteur de projet (particulier, association, entreprise, etc.) de financer celui-ci grâce à une collecte de dons, se faisant en général sur une plateforme Internet. C'est un mode de fonctionnement _désintermédié_, c'est-à-dire qu'il n'utilise pas les acteurs du financement classiques (banques, etc.)

Dans le *crowdfunding*, un financement peut se faire de plusieurs manières :
* Le don : dans ce cas, le public finance le projet sans contrepartie financière. Il peut cependant exister des contreparties non-financières : cadeau, pré-achat du produit, abonnement pendant un certain temps ou à vie au service financé, etc.
```mermaid
graph LR
 	Financeur-->|Don|Projet
  	Projet-->|Contrepartie|Financeur
```
* Le prêt : la personne permet le financement du projet en prêtant de l'argent au porteur de projet. Il peut y avoir des intérêts ou non. Dans ce cas, la somme engagée est censée être rendue.
```mermaid
graph LR
	Financeur-->|Prêt|Projet
	Projet-->|Intérêts|Financeur
```
* L'investissement participatif : en échange de son investissement, l'investisseur devient actionnaire ou perçoit des intérêts.
```mermaid
graph LR
	Financeur-->Projet
	Projet-->|Actions, intérêts, etc.|Financeur
```

Le *crowdfunding* peut concerner des particuliers, des associations, des entreprises, etc.

### Bref historique

Bien que le *crowdfunding* se soit beaucoup développé avec le Web et les plateformes de financement en ligne, celui-ci remonte à avant la création d'internet.
* _1875_ : première levée de fond pour la construction de la Statue de la Liberté (160 000 contributeurs français et américains)
* _1885_ : la construction de la Sagrada Familia débute, uniquement grâce à des dons anonymes.
* _Années 90_ : apparition d'Internet
	* _1997_ : Financement participatif d'une tournée d'un groupe de rock anglais : Marillion.
	* _1997_ : Naissance de My Major Company, label participatif pour les artistes musicaux.
* _2008 et 2011_ : Financement participatif d'une partie de la campagne présidentielle d'Obama (118 000 000 $ récoltés en 2011 sur sont site Internet).
* 2009 : Création de [Kickstarter](https://www.kickstarter.com/), plateforme en ligne de *crowdfunding*.
* 2012 : Mise en place du JOBS Act (*Jumpstart Our Business Startups Act*), premier cadre légal du *crowdfunding*.

*[(Source)](https://www.homunity.com/fr/crowdfunding)*

Nous pouvons ainsi voir que le *crowdfunding* peut concerner quasiment tous les domaines : architecture, musique, politique, etc. Il peut aussi permettre le financement d'entreprises (startups notamment), le financement de projets (lancement de produits, création d'un produit écologique, etc.), de recherche scientifique, etc.

### Quelques chiffres du crowdfunding

Les graphiques suivants ont été réalisés avec les [statistiques de la plateforme Ulule](https://fr.ulule.com/stats/) (plateforme de *crowdfunding* française).

Le taux de succès correspond au rapport du nombre de projets dont l'objectif de financement sur la plateforme a atteint 100%, sur le nombre de projets fermés (financement terminé, abandonné, annulé, etc.).

![alt Fonds collectés par Ulule](/img/ulule_fonds_collectes.PNG "Fonds collectés par Ulule")

![Proportion de fonds collectés par catégorie de projet](/img/ulule_proportion_par_categorie.PNG "Proportion de fonds collectés par catégorie")

![Taux de succès](/img/ulule_taux_succes.PNG "Taux de succès")

Tout d'abord, on peut remarquer une tendance à la hausse de ce type de financement.

Nous pouvons voir que les projets couvrent beaucoup de domaines. Cependant, les chiffres d'Ulule ne correspondent pas forcément à la tendance globale : la majorité des fonds sont en effet investis dans l'immobilier [(source)](https://www.moneyvox.fr/actu/78316/crowdfunding-5-chiffres-etonnants-sur-le-succes-du-financement-participatif).

De plus, nous pouvons observer que le taux de succès n'est pas toujours très élevé. Cependant, cela n'est pas à rattacher directement à un manque d'efficacité du *crowdfunding*, cela peut être simplement dû au fait que les projets n'attirent pas toujours des investisseurs ou donateurs.

### Exemples

Voici quelques exemples de succès du *crowdfunding* :
* Oculus VR (casques de réalité virtuelle) a au départ été financé par une campagne de *crowdfunding* sur Kickstarter. Cette campagne a permis de récolter 2 437 429 dollars américains. Oculus VR a été racheté par Facebook en 2014 [(source)](https://fr.wikipedia.org/wiki/Oculus_VR).
* Le jeu vidéo *Star Citizen* a été financé via une campagne Kickstarter. Il a battu des records en terme d'argent collecté grâce au *crowdfunding* avec plus de 300 millions de dollars récoltés.

### Avantages du crowdfunding

Le *crowdfunding* permet une alternative aux méthodes de financement classiques (banques, etc.), notamment dans les cas suivants :
* Refus du financement d'un projet par une banque ou des investisseurs classiques
* Projets souhaitant avoir un financement "propre" (écologiquement, socialement, etc.), sans passer par des banques pour qui les fonds d'investissement proviennent parfois d'entreprises peu regardantes sur l'environnement, sur l'éthique, etc.
* Projets innovants, le *crowdfunding* étant lui-même qualifié d'innovant.
* Utilisation du *crowdfunding* en complément des méthodes de financement classiques.

De plus (et c'est la raison pour laquelle le *crowdfunding* est mentionné dans ce chapitre), le financement participatif est une alternative aux domaines économiques dominants et principalement les banques. De même que pour le domaine du libre (logiciel, licences libres, etc.) ou de l'*open source*, il permet un financement collaboratif sans passer par les géants du Web. C'est de plus une bonne méthode de financement pour les projets libres, qui permet la transparence, etc.

### Inconvénients du crowdfunding

Le *crowdfunding* présente néanmoins certains inconvénients :
* Des commissions parfois élevées sur certaines plateformes (exemple : MyMajorCompany prélève 10% de commissions sur les montants collectés)
* Contrairement aux financeurs classiques, il faut surtout miser sur la communication dans une campagne de *crowdfunding*.
* Le financement est incertain : si la campagne ne fonctionne pas et que les palliers ne sont pas atteints, aucune somme n'est collectée.
* En lançant une campagne de *crowdfunding*, on rend publique son projet. Celui-ci peut donc être reproduit par d'autres personnes ou entreprises.

*Sources :*
* https://fr.wikipedia.org/wiki/Financement_participatif
* https://www.homunity.com/fr/crowdfunding
* https://www.economie.gouv.fr/entreprises/crowdfunding-financement-participatif